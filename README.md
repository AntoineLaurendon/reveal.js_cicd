# Documentation du Projet reveal.js CI/CD
*Auteur : Antoine LAURENDON, TSI promotion 2019-2020*

## Présentation - Objectifs du projet
Le but de ce projet d'intégration continue et de déploiement continu est de mettre en oeuvre les connaissances acquises lors de deux journées complètes de cours.
Le principe est de mettre en place une intégration continue sur un projet.

Le TP s'effectuera sur le projet reveal.js. Il s'agit d'un framework Javascript pour créer des présentations en HTML et CSS. D'autres fonctionnalités viennent enrichir l'outil (exemple : export du diaporama en PDF). Il s'agit d'un projet libre et opensource protégé par une licence MIT, donc sa réutilisation est parfaitement permise. 

Vous retrouverez le dépôt Github du projet ici : https://github.com/hakimel/reveal.js


## Technologies utilisées
Étant libre sur les technologies à utiliser, j'ai décidé de travailler à l'aide du Gestionnaire de Code Source Gitlab, car c'est un outil que je connais, et donc le temps passé à s'approprier les fonctionnalités à utiliser est reste moindre. Un outil Gitlab CI sous un format Cloud est par ailleurs intégré. Il s'agit donc d'une solution complète pour réaliser toute la chaîne d'opérations. Ses fonctionnalités vont également jusqu'à la l'hébergement d'un site web statique : nous l'utiliserons dans la suite du projet, au cours de la phase de déploiement


## Présentation du pipeline
Le pipeline est utilisé pour assurer l'intégration continue et le déploiement en continu du projet. Il s'agit d'un ensemble de traitement qui visent à déployer des versions finales du projet sur des pas de temps plus rapprochés. 
L'intégration continue consiste à apporter régulièrement des modifications au code d'une application. 
Le déploiement continu désigne plutôt le transfert d'un environnement de développement ou de pré-production, à un environnement de production, correspondant à la version finale à disposition des utilisateurs.

Le pipeline réalisé contient donc 3 étapes principales : 
- Le build, correspondant à la phase de construction de l'application
- Les tests, de manière à s'assurer que le code ne contient pas d'erreur (tests unitaires, tests d'intégration, tests d'acceptance par exemple)
- Le déploiement du logiciel sur un site web statique

Ce pipeline est disponible à la racine du dépôt Gitlab : *.gitlab-ci.yml*

### Build
Le framework reveal.js est une application Node.js. *npm* est un outil pour aider au développement, aux tests, et au déploiement des applications Node.js. 
Toutes les commandes sont réalisées dans le fichier *CI_CD/build.sh*
Le build est effectué parallèlement sur les versions 8 et 10 de Node.js. 
En parallèle de la construction de l'application Node.js, une documentation est générée à l'aide du framework Docsify.
Retrouvez la documentation à l'adresse suivante : https://docsify.js.org/#/quickstart. 

La documentation en ligne est générée, mais visible seulement à l'aide de l'intégration de la commande ***docsify serve docs***. 


### Tests
Toujours dans notre pipeline, les tests sont réalisés simplement à l'aide de la commande ***npm test***, qui fait référence aux informations contenues dans le fichier de configurtion **package.json**. A l'aide de cette simple commande, le programme lance les tests qui lui sont propres.
Dans le fichier **.yml**, le pipeline est enrichi de plusieurs autres lignes de commande pour ce stage **Build** en raison des dépendances aux librairies utilisées. 
De la même façon que pour le build, les test sont réalisés à l'aide des versions 8 et 10 de Node.js

### Déploiement - problèmes rencontrés
Mon objectif pour la partie déploiement du site web statique, mon objectif était de créer une Gitlab Page. J'ai très rapidement été bloqué, car la solution Gitlab Page ne peut accueil qu'un seul site web statique par projet. J'ai donc tenté de contourner le problème, et de créer dans le dépôt *public* à la racine du projet un dossier par branche. Le principes étaient les suivants : 
- à chaque création d'une branche, un dossier *<branch_name>* est créé, contenant la globalité des fichiers et dossier de la branche.
- lors d'un commit sur une branche autre que la branche master, une nouvelle version du site web statique est générée.
- lors d'un merge d'une branche vers la branche master, le site web statique de la branche master est mis à jour avec la branche mergée.

J'ai donc passé un bon nombre d'heures, notamment le jour du rendu, à tenter de résoudre les problèmes... J'étais notamment confronté au souci de la gestion du cache. A chaque nouveau commit, la création d'une nouvelle arborescence public/master ou public <ma_branche> créant une arborescence infinie public/master/public/master/public/master/etc...

L'arborescence que j'aurais souhaité mettre en place est la suivante : 

- master
    - public
        - master
            - repository complet de master
        - <ma_branche1>
            - repository complet de <ma_branche1>
        - <ma_branche2>
            - repository complet de <ma_branche2>
- <ma_branche1>
- <ma_branche2>

A défaut d'obtenir une version finie de la solution, l'étape déploiement génère le site statique lorsqu'une action de type commit, merge, ou création de branche (seulement le statique de la branche en question, impactée par l'action).

